﻿
#include <iostream>
#include <ctime>
using namespace std;
const int Limit = 7;

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int DayOfMonth = buf.tm_mday;
	const int NumberOfLine = DayOfMonth % Limit;

	int DoubleArray[Limit][Limit]{};
	int SumOfElement = 0;
	for (int i = 0; i < Limit; i++)
	{
		for (int j = 0; j < Limit; j++)
		{
			DoubleArray[i][j] = i + j;
			cout << DoubleArray[i][j] << " ";
		}
		cout << '\n';
	}
	
	for (int j = 0; j < Limit; j++)
	{
		SumOfElement+= DoubleArray[NumberOfLine][j];
	}
	cout << SumOfElement;
	 
}


